/**
 * GoogleMaps base URL.
 */
const URL = 'https://www.google.fr/maps';

/**
 * Puppeteer viewport (px)
 */
const VIEWPORT = {
  WIDTH: 224,
  HEIGHT: 224
};

/**
 * Puppeteer delays (ms) between each operation.
 */
const DELAYS = {
  DOM: 2000,
  INPUTS: 700,
  SCREENSHOT: 1300,
};

/**
 * Inputs parameters.
 */
const INPUTS = {
  V_PRESS_COUNT: 1,
  H_PRESS_COUNT: 1
};

/**
 * Various CSS selectors.
 * This should be updated if GoogleMaps DOM changes.
 * Order of SELECTORS.CLICK selectors is important.
 */
const SELECTORS = {
  CLICK: {
    // USECONDITIONS: '#pushdown > div > div > a.gb_Ld.gb_ld',
    IGNORECONSENT: '#consent-bump > div > div.widget-consent-dialog > span > button.widget-consent-button-later.ripple-container',
    MINIMAP: '#minimap > div > div.widget-minimap > button',
    MENU1: '#omnibox-singlebox > div.omnibox-singlebox-root.omnibox-active > div.searchbox-hamburger-container > button',
    SAT: '#settings > div > div.widget-settings-pane > div > ul:nth-child(3) > li:nth-child(2) > div > button.widget-settings-button.widget-settings-earth-button.widget-settings-intent-off > label',
    MENU2: '#omnibox-singlebox > div.omnibox-singlebox-root.omnibox-active > div.searchbox-hamburger-container > button',
    HIDELABELS: '#settings > div > div.widget-settings-pane > div > ul:nth-child(3) > li:nth-child(2) > div > button:nth-child(2) > label.widget-settings-sub-button-label'
  },
  CLICK3D: {
    GLOBE: '#globe-toggle > div > button > div',
    TILT: '#tilt > div > button > div.widget-tilt-button-text > div:nth-child(1) > span',
    // COMPASS: '#compass > div > button.compass-clockwise-arrow.widget-compass-sprite'
  },
  DOM: {
    ZOOMBUTTONS: '#content-container > div.app-viewcard-strip',
    HIDEPANEBUTTON: '#pane > div > div.widget-pane-toggle-button-container',
    FOOTER: '#content-container > div.scene-footer-container.noprint',
    LOGIN: '#gb',
    WATERMARK: '#watermark',
    OMNIBOX: '#omnibox-container'
  },
  DOM3D: {
    OMNIBOX: '#omnibox-container',
    VASQUETTE: '#vasquette'
  }
};

/**
 * Various parsers for GoogleMaps URLs.
 */
const PARSERS = {
  LAT: (url) => parseFloat(url.split('/')[4].split(',')[0].substring(1)),
  LON: (url) => parseFloat(url.split('/')[4].split(',')[1])
};

/**
 * Some command lines.
 * node index.js 50.962590 1.829577 50.934233 1.909539 17 calais_full
 * node index.js 50.961241 1.818936 50.934648 1.904000 21 calais_full
 */

module.exports = { URL, VIEWPORT, DELAYS, INPUTS, SELECTORS, PARSERS };