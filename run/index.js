var fs = require('fs');
const puppeteer = require('puppeteer');
const { PendingXHR } = require('pending-xhr-puppeteer');

const { URL, VIEWPORT, DELAYS, INPUTS, SELECTORS, PARSERS } = require('./conf');

/**
 * Command line.
 */
if (process.argv.length < 8) {
  return console.log(`usage: node index.js <topleft_lat> <topleft_lon> <botright_lat> <botright_lon> <zoom> <out> [3d]
    - topleft_lat: latitude of the starting point (top left of the bbox)
    - topleft_lon: longitude of the starting point (top left of the bbox)
    - botright_lat: latitude of the end point (bot right of the bbox)
    - botright_lon: longitude of the end point (bot right of the bbox)
    - zoom: GoogleMaps zoom (1-21)
    - out: output folder name
    - 3d: boolean for 3D view`);
}

/**
 * Process argv.
 */
const TL_LAT = process.argv[2];
const TL_LON = process.argv[3];
const BR_LAT = parseFloat(process.argv[4]);
const BR_LON = parseFloat(process.argv[5]);
const ZOOM = process.argv[6];
const OUT = process.argv[7];
const THREED = process.argv[8] || false;
const PAGE = `${URL}/@${TL_LAT},${TL_LON},${ZOOM}z/data=!3m1!1e3`;

/**
 * Make output folder.
 */
if (!fs.existsSync(OUT)){
  fs.mkdirSync(OUT);
}

(async () => {
  /**
   * Start browser.
   */
  const browser = await puppeteer.launch({
    headless: false,
  });
  const page = await browser.newPage();
  const pendingXHR = new PendingXHR(page);
  
  /**
   * Set viewport.
   */
  await page.setViewport({
    width: VIEWPORT.WIDTH,
    height: VIEWPORT.HEIGHT
  });

  /**
   * Open GoogleMaps at the specified location.
   */
  await page.goto(PAGE);
  await page.waitForNavigation({ waitUntil: 'networkidle0' })

  /**
   * Click everywhere and hide shit.
   */
  for (let s of Object.values(SELECTORS.CLICK)) {
    await page.waitFor(DELAYS.DOM);
    await page.click(s);
  }

  /**
   * Click everywhere and hide shit (3D view).
   */
  if (THREED) {
    await page.evaluate(
      (selectors) => {
        Object.values(selectors).map(
          (s) => {
            const node = document.querySelector(s);
            if (node) {
              node.parentNode.removeChild(node);
            }
          });
      }, SELECTORS.DOM3D);
    for (let s of Object.values(SELECTORS.CLICK3D)) {
      await page.waitFor(DELAYS.DOM);
      await page.click(s);
    }
  }

  /**
   * Remove more shit.
   */
  await page.evaluate(
    (selectors) => {
      Object.values(selectors).map(
        (s) => {
          const node = document.querySelector(s);
          if (node) {
            node.parentNode.removeChild(node);
          }
        });
    }, SELECTORS.DOM);

  /**
   * Scrape.
   */
  // Focus on map
  await page.waitFor(DELAYS.DOM);
  await page.mouse.click(VIEWPORT.WIDTH / 2, VIEWPORT.HEIGHT / 2);
  await page.waitFor(DELAYS.INPUTS * 2);
  await page.keyboard.press('ArrowRight');
  await page.waitFor(DELAYS.INPUTS);
  await page.keyboard.press('ArrowLeft');
  await page.waitFor(DELAYS.DOM);
  // Move in a grid
  let tile = 0;
  let isArrowRight = true;
  let current_lat = PARSERS.LAT(page.url());
  let current_lon = PARSERS.LON(page.url());
  while (current_lat >= BR_LAT) {
    console.log('Next row...');
    while ((isArrowRight && current_lon <= BR_LON) || (!isArrowRight && current_lon >= TL_LON)) {
      // Screenshot
      await pendingXHR.waitForAllXhrFinished();
      await page.waitFor(DELAYS.SCREENSHOT);
      const url = page.url();
      const path = url.split('/')[4].replace(/,/g, '-').replace(/\./g, ',');
      console.log('Screen', tile, path);
      await page.screenshot({
        path: `${OUT}/${OUT}_${String(tile++).padStart(5, '0')}${path}.jpg`
      });
      // Move horizontally
      for (let press=0; press<INPUTS.H_PRESS_COUNT; press++) {
        await page.waitFor(DELAYS.INPUTS);
        await page.keyboard.press(isArrowRight ? 'ArrowRight' : 'ArrowLeft');
      }
      // Update camera position
      current_lat = PARSERS.LAT(page.url())
      current_lon = PARSERS.LON(page.url());
    }
    // Move vertically
    for (let press=0; press<INPUTS.V_PRESS_COUNT; press++) {
      await page.waitFor(DELAYS.INPUTS);
      await page.keyboard.press('ArrowDown');
    }
    // Change horizontal direction
    isArrowRight = !isArrowRight;
    // Move horizontally
    for (let press=0; press<INPUTS.H_PRESS_COUNT; press++) {
      await page.waitFor(DELAYS.INPUTS);
      await page.keyboard.press(isArrowRight ? 'ArrowRight' : 'ArrowLeft');
    }
  }

  /**
   * Close browser.
   */
  await browser.close();
})();
