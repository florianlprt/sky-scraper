# sky-scraper

Scrap satellite images from GoogleMaps.

## setup

```sh
git clone https://gitlab.com/florianlprt/sky-scraper.git
cd sky-scraper/
cd run/
npm install
```

## run

```
node index.js <topleft_lat> <topleft_lon> <botright_lat> <botright_lon> <zoom> <out>

params:
    - topleft_lat: latitude of the starting point (top left of the bbox)
    - topleft_lon: longitude of the starting point (top left of the bbox)
    - botright_lat: latitude of the end point (bot right of the bbox)
    - botright_lon: longitude of the end point (bot right of the bbox)
    - zoom: GoogleMaps zoom (1-21)
    - out: output folder name
```

## Demo

![](public/videos/skyscraper.mp4)